import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import FilePreview from '@mrhanson/vue-file-preview'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(Vuetify);
Vue.use(FilePreview)
Vue.use(VueSweetalert2);

export default new Vuetify({
    icons: {
        iconfont: 'md',
    },
});