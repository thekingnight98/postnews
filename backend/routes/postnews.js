const router = require('express').Router();
let PostNews = require('../models/News.model');

router.route('/').get((req, res) => {
    PostNews.find()
        .then(news => res.json(news))
        .catch(err => res.status(400).json('Error ' + err))
});


// เพิ่ม ข่าว
router.route('/add').post((req, res) => {
    const title = req.body.title;
    const detail = req.body.detail;
    const category = req.body.category;
    // const tags = req.body.tag;
    // const tags = req.body.tags.map((tag) => {
    //     return { "text": tag }
    // })
    // const imageUrl = req.body.imageUrl;
    const tags = req.body.tags.map(tag => tag.text);



    const postNews = new PostNews({
        title,
        detail,
        category,
        // imageUrl,
        tags
    });

    postNews.save()
        .then(() => res.json('News added !'))
        .catch(err => res.status(400).json('Error ' + err));
});

// หา news by id
router.route('/:id').get((req, res) => {
    PostNews.findById(req.params.id)
        .then((news) => res.json(news))
        .catch(err => res.status(400).json('Error ' + err));
});


// ลบ news by id
router.route('/:id').delete((req, res) => {
    PostNews.findByIdAndDelete(req.params.id)
        .then(() => res.json('News deleted !'))
        .catch(err => res.status(400).json('Error ' + err));
});

// อัปเดต news by id
router.route('/update/:id').post((req, res) => {
    PostNews.findById(req.params.id)
        .then(news => {
            news.title = req.body.title;
            news.detail = req.body.detail;
            news.category = req.body.category;
            news.tags = req.body.tags;

            news.save()
                .then(() => res.json('Post News updated !!'))
                .catch((err) => res.status(400).json('Error ' + err));
        })
        .catch((err) => res.status(400).json('Error ' + err));

});

module.exports = router;